/*
	Welcome to the config file!
	Add keys here to override values from /src/lib/constants.js. Please look at that file for override recommendations.
	This file should hopefully never be altered upstream.
	You must restart Bibliogram to apply these changes.
*/

module.exports = {
	website_origin: "https://bib.minguyen.de",
	port: 10407,
	does_not_track: true,
    has_privacy_policy: true,
	tor: {
		enabled: true
	},
	feeds: {
		enabled: true
	},
	default_user_settings: {
		rewrite_youtube: "invidious.snopyta.org", 
		rewrite_twitter: "nitter.minguyen.de"
	}
}
